# PI-Rail-FX

PI-Rail client App written in JavaFX, based on PI-Rail-Client project.

![PI-Rail App screenshot](images/Screenshot_Test-Anlage.png) 

## Documentation

- [Technical Documentation (english)](https://pi-rail.gitlab.io/pi-rail-doc)
- [User Documentation (german)](https://ctc-system.gitlab.io/ctc-doku)
- See [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on module firmware.
- See also [PI-Rail.org (german wiki)](https://www.pi-rail.org). 

## Getting started

**If you just want to get the Client app for your CTC modules see [CTC download page](https://ctc-system.gitlab.io/ctc-doku/de/download.html).** 

If you want to start your own project or (better) contribute

1. Create a directory for your PI-Rail projects
1. Check out all PI-Mobile and PI-Rail projects to that directory.
   Each project should be located in a directory having the same name like the repository.
   You need the following directories (repositories):
   - [pi-mobile-core](https://gitlab.com/pi-mobile/pi-mobile-core)
   - [pi-mobile-fx](https://gitlab.com/pi-mobile/pi-mobile-fx)
   - [pi-rail-base](https://gitlab.com/pi-rail/pi-rail-base)
   - [pi-rail-client](https://gitlab.com/pi-rail/pi-rail-client)
   - [pi-rail-fx](https://gitlab.com/pi-rail/pi-rail-fx)
   - [Z21-Drive](https://github.com/PI-Data/z21-drive)
1. Open "pi-rail-fx" project with IntelliJ IDEA.

## How to contribute?

There are many ways you can help this project. There is a lot of work to do:
- Documentation.
- Testing and Bug reporting.
- Integrate with existing model railway control software
- Integrate with classic digital railway systems (e.g. DCC, mfx)
- Integrate with other WiFi and or ESP32 based open source projects 
- Collect ideas for future software features.
- ...

Interested? - Just write a mail in german or english to [Peter Rudolph](mailto:peter.rudolph@pi-data.de)
