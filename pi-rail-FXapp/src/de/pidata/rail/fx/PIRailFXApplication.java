package de.pidata.rail.fx;

import de.pidata.file.FilebasedRollingLogger;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.guidef.ControllerFactory;
import de.pidata.gui.javafx.FXApplication;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.rail.model.PiRailFactory;
import de.pidata.rail.railway.RailwayFactory;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.system.desktop.DesktopSystem;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Optional;

public class PIRailFXApplication extends FXApplication {

  public void init() throws Exception {
    try {
      File jarFile = new File( getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
      this.basePath = jarFile.getParentFile().getAbsolutePath();
    }
    catch (URISyntaxException e) {
      // do nothing - basePath stays "."
    }
    Platform.loadServices = false;
    new FXFactoryPIRail(); // replace UI Factory
    try {
      File appDir = new File( DesktopSystem.getPathAppData(), "CTC-App" );
      File logDir = new File( appDir, "logs" );
      logDir.mkdirs();
      File logFile = new File( logDir, "pi-rail.log" );
      FilebasedRollingLogger streamLogger = new FilebasedRollingLogger( logFile.getAbsolutePath(), 1, 30, Level.DEBUG );
      Logger.addLogger( streamLogger );
    }
    catch (Exception ex) {
      Logger.error( "Error creating log file", ex );
    }
    super.init();
    ModelFactoryTable factoryTable = ModelFactoryTable.getInstance();
    factoryTable.getOrSetFactory( BaseFactory.NAMESPACE, BaseFactory.class );
    factoryTable.getOrSetFactory( ControllerFactory.NAMESPACE, ControllerFactory.class );
    factoryTable.getOrSetFactory( PiRailFactory.NAMESPACE, PiRailFactory.class );
    factoryTable.getOrSetFactory( PiRailTrackFactory.NAMESPACE, PiRailTrackFactory.class );
    factoryTable.getOrSetFactory( RailwayFactory.NAMESPACE, RailwayFactory.class );
  }

  @Override
  public void start( Stage primaryStage ) throws Exception {

    primaryStage.setX( 0.0 );
    primaryStage.setY( 0.0 );
    primaryStage.getIcons().add( new Image( getClass().getResource( "/drawable/clever_train_control_App_Icon_152px.png" ).toString() ) );

    super.start( primaryStage );
    /*FXDockingDialog fxDialog = new FXDockingDialog( initialDialogID, primaryStage, 0 );
    fxDialog.load();
    rootDlgCtrl = platform.getControllerBuilder().createDialogController( initialDialogID, initialContext, fxDialog, null );
    rootDlgCtrl.setParameterList( createInitialParamList() );

    fxDialog.show( null, false );
    rootDlgCtrl.activate( fxDialog, null );
    */

    primaryStage.setOnCloseRequest( closeEvent -> {
      closeEvent.consume();
      askForClosing();
    } );
  }

  private void askForClosing() {
    Alert alert = new Alert( Alert.AlertType.CONFIRMATION );
    alert.setTitle( "Beenden" );
    alert.setHeaderText( "Wollen Sie die App wirklich beenden?" );
    Optional<ButtonType> result = alert.showAndWait();
    if (result.get() == ButtonType.OK) {
      rootDlgCtrl.close( true );
      System.exit( 0 );
    }
  }

  public static void main( String[] args ) {
    args = new String[1];
    args[0] = "Application";
    launch( args );
  }
}
