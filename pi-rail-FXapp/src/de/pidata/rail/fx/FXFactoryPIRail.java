package de.pidata.rail.fx;

import de.pidata.gui.chart.ChartViewPI;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.javafx.chart.FXChartAdapter;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.scene.chart.LineChart;

public class FXFactoryPIRail extends FXUIFactory {

  @Override
  public UIAdapter createCustomAdapter( UIContainer uiContainer, ViewPI viewPI ) {
    if (viewPI instanceof ChartViewPI) {
      LineChart lineChart = (LineChart) findUIComp( uiContainer, viewPI.getComponentID(), viewPI.getModuleID() );
      if (lineChart == null) {
        throw new IllegalArgumentException( "Could not find UI component, ID="+viewPI.getComponentID() );
      }
      return new FXChartAdapter( lineChart, (ChartViewPI) viewPI, uiContainer );
    }
    else {
      return super.createCustomAdapter( uiContainer, viewPI );
    }
  }
}
