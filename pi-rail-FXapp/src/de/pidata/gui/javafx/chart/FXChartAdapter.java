package de.pidata.gui.javafx.chart;

import de.pidata.gui.chart.ChartViewPI;
import de.pidata.gui.chart.UIChartAdapter;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.ui.FXAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.HashMap;
import java.util.Map;

public class FXChartAdapter extends FXAdapter implements UIChartAdapter {

  private LineChart lineChart;
  private ChartViewPI chartViewPI;
  private Map<String,XYChart.Series> seriesMap = new HashMap<>();

  public FXChartAdapter( LineChart lineChart, ChartViewPI chartViewPI, UIContainer uiContainer ) {
    super( lineChart, chartViewPI, uiContainer );
    this.lineChart = lineChart;
    this.chartViewPI = chartViewPI;
    lineChart.setCreateSymbols( false );
  }

  @Override
  public void addSeries( String name ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        XYChart.Series series = new XYChart.Series();
        series.setName( name );
        lineChart.getData().add( series );
        seriesMap.put( name, series );
      }
    } );
  }

  /**
   * Disable auto range for Y Axis and fix to given range
   *
   * @param lowerBound new lower bound for Y-Axis
   * @param upperBound new upper bound for Y-Axis
   */
  @Override
  public void setYAxis( double lowerBound, double upperBound ) {
    NumberAxis axis = (NumberAxis) this.lineChart.getYAxis();
    axis.setAutoRanging( false );
    axis.setLowerBound( lowerBound );
    axis.setUpperBound( upperBound );
  }

  @Override
  public void addData( String name, String x, int y ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        XYChart.Series series = seriesMap.get( name );
        if (series != null) {
          series.getData().add( new XYChart.Data( x , y ) );
        }
      }
    } );
  }

  @Override
  public void addData( String name, int x, int y ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        XYChart.Series series = seriesMap.get( name );
        if (series != null) {
          series.getData().add( new XYChart.Data( x , y ) );
        }
      }
    } );
  }

  /**
   * Remove first count data from series name
   * @param name  name of the series,  null for all
   * @param count amount of entries to remove
   */
  public void removeData( String name, int count ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (name == null) {
          for (XYChart.Series series : seriesMap.values()) {
            series.getData().remove( 0, count );
          }
        }
        else {
          XYChart.Series series = seriesMap.get( name );
          if (series != null) {
            series.getData().remove( 0, count );
          }
        }
      }
    } );
  }

  /**
   * Removes all data from all series, but does not remove the series itself.
   */
  @Override
  public void clearData() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        for (XYChart.Series series : seriesMap.values()) {
          series.getData().clear();
        }
      }
    } );
  }
}
